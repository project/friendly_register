<?php

/**
 * @file
 * Contains \Drupal\friendly_register\Controller\FriendlyRegisterController.
 */

namespace Drupal\friendly_register\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class FriendlyRegisterController extends ControllerBase {
  public function checkEmail($address) {
    $users = $this->entityTypeManager()->getStorage('user')
      ->loadByProperties(['mail' => $address]);

    return new JsonResponse([
      'available' => (!(bool) reset($users))
    ]);
  }

  public function checkUser($username) {
    $users = $this->entityTypeManager()->getStorage('user')
      ->loadByProperties(['name' => $username]);

    return new JsonResponse([
      'available' => (!(bool) reset($users))
    ]);
  }
}
